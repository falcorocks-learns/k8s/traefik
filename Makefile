install:
	helm repo add traefik https://helm.traefik.io/traefik
	helm repo update
	helm install traefik traefik/traefik --values values.yaml

upgrade:
	helm upgrade traefik traefik/traefik --values values.yaml
	helm get manifest traefik > traefik/manifest.yaml
